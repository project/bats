Book Auto Terms (BATS) allows you to assign taxonomy terms for book pages based 
on the parent book page.

HISTORY
----------

The Book Auto Terms (BATS) module was created initially to automatically assign 
the appropriate content editors of a content page based on where in the site 
map the page was placed.  Since the Taxonomy Access Control module was currently 
being used to manage viewing and editing permissions, and the Book module was 
being used for simple hierarchical site maps, the task became to automatically
select a vocabulary term based on the parent book page selected.

During the development of this initial task, it quickly became apparent that 
a module that gave us the functionality we needed could easily be extended to 
automatically assign many other things, including the following:

- Who can view a page.
- Who can edit a page.
- RSS categories and tags.
- General taxonomy terms.
- Photo metadata.

INSTALLATION
------------

Copy all files to modules/bats/, visit Admin >> Site building >> Modules 
(admin/build/modules) and enable Book Auto Terms. The provided install file 
should correctly set the weight of the module. If not execute the following 
query manually:

UPDATE system SET weight = -1 WHERE name = 'bats';

CONFIGURATION
-------------

To configure BATS, one must associate an array of BATS_Vocab objects with the 
BATS module.  The BATS_Vocab object is a container for a single vocabulary id, 
a single term id that when selected the BATS module will run, and the map of 
node id's to term id's.

The configuration of BATS is currently done manually by entering a 
'bats_vocab.php' file into the root of the active theme directory.  The 
contents of this file is the assignment of a $vocabs variable to an array of 
BATS_Vocab objects.

Plans for future versions of this module include an admin settings page where 
the assignment of terms to selected parent book pages can be done with a GUI.  
The 'bats_vocab.php' file will most likely always override any GUI settings.

It's important to note that term assignment cascades from parent to child.  
This means that one must only assign a minimal number of terms to specific 
book pages.  Any child pages of a parent, no matter how many levels down, 
will inherit the assigned term of the parent.  This also means that at a 
minimum, the root book page of a book MUST have an assigned term.  In this 
regard, this module doubles as a replacement for the Taxonomy Defaults module.

SAMPLE bats_vocab.php
---------------------

<?php

$vocabs = array(
	/*
	 * new BATS_Vocab(int $vocabID, int $autoSelectTermID, array $termMap);
	 */

	/* Automatically assign editors */
	new BATS_Vocab(3, 43, array( // nid => tid
		1 => 41, 			// Main Page => Content Admin
		3 => 32, 			// Major Section 1 => Content Editor 1
		4 => 33,			// Major Section 2 => Content Editor 2
		5 => array(29,41)	// Major Section 3 => Content Editors 3 and 4
	)),
	
	/* Automatically assign viewers */
	new BATS_Vocab(2, 58, array( // nid => tid 
		 1 => 50, 				// Main Page => Everyone
		 6 => 51, 				// Private Section 1 => Group 1
		 7 => array(52, 53),	// Private Section 1 => Groups 2 and 3
		 8 => 54				// Private Section 3 => Group 4
	))
);

?>
